from flask import Flask, make_response
from flask.ext.restful import Api
from flask.ext.mongoengine import MongoEngine

app = Flask(__name__)
app.config.from_object('settings')
db = MongoEngine(app)
api = Api(app, prefix='/api/v1')


@api.representation('application/json')
def json_representation(data, code, *args, **kwargs):
    resp = make_response(data, code)
    return resp


def register_blueprints():
    from app.modules.core import core
    from app.modules.account import account
    from app.modules.news import news
    from app.modules.admin import admin

    app.register_blueprint(core, url_prefix='/')
    app.register_blueprint(account, url_prefix='/account')
    app.register_blueprint(news, url_prefix='/news')
    app.register_blueprint(admin, url_prefix='/admin')

register_blueprints()
