from flask.ext.restful import Resource

from app import api
from .models import NewsItem, Category


class CategoriesListResource(Resource):

    def get(self):
        categories = Category.objects
        return categories.to_json()


class NewsListResource(Resource):

    def get(self):
        news = NewsItem.objects
        return news.to_json()

    def post(self):
        return {}


class NewsItemResource(Resource):

    def get(self, item_id):
        return {}

    def delete(self):
        return {}, 204

api.add_resource(NewsListResource, '/news/')
api.add_resource(NewsItemResource, '/news/<item_id>/')

api.add_resource(CategoriesListResource, '/categories/')
