from app import db


class Category(db.Document):
    """
    Category model
    """
    title = db.StringField(required=True, unique=True, max_length=120)
    order = db.IntField(default=0)
    created = db.DateTimeField()
    updated = db.DateTimeField()

    meta = {
        'order': ['order'],
        'collection': 'categories'
    }


class NewsItem(db.Document):
    """
    News item model
    """
    title = db.StringField(required=True, unique=True, max_length=120)
    category = db.ReferenceField(Category)
    content = db.StringField(required=True)
    published = db.BooleanField(default=False)
    created = db.DateTimeField()
    updated = db.DateTimeField()

    meta = {
        'ordering': ['-created'],
        'collection': 'news'
    }
