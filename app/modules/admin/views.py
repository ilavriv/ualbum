from app.modules.news.models import Category, NewsItem
from contrib.views import (
    ListView,
    FormView
)

from . import admin


class CategoriesListView(ListView):
    model = Category
    template_name = 'admin/categories.html'


class NewsListView(ListView):
    model = NewsItem
    template_name = 'admin/news.html'


class CategoryCreateView(FormView):
    pass


admin.add_url_rule(
    '/categories',
    view_func=CategoriesListView.as_view('categories')
)

admin.add_url_rule(
    '/news',
    view_func=NewsListView.as_view('news')
)

admin.add_url_rule(
    '/categories/new',
    view_func=CategoryCreateView.as_view('new_category'))
