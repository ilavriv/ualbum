from exceptions import NotImplementedError

from flask import render_template, request
from flask.views import View


class TemplateView(View):
    """
    Simple template view
    """
    def get_template_name(self):
        """
        Needs implement this method
        """
        raise NotImplementedError()

    def get_context(self):
        """
        Needs implement this method
        """
        raise NotImplementedError()

    def dispatch_request(self, *args, **kwargs):
        print self.get_template_name()
        context = self.get_context()
        return render_template(self.get_template_name(), **context)


class ListView(TemplateView):
    """
    display list of model items
    """
    template_name = None
    model = None

    def get_queryset(self):
        """
        Returns queryset
        """
        return self.model.objects

    def get_template_name(self):
        return self.template_name

    def get_context(self):
        return {
            'queryset': self.get_queryset()
        }


class FormView(TemplateView):
    """"
    Form view for create and update
    """
    template_name = None
    form_class = None
    model = None

    def get_form(self):
        return self.form()

    def get_object(self, id):
        return self.model.find(id)

    def get_template_name(self):
        return self.template_name

    def get_context(self):
        """
        Generate form context
        """
        id = request.params['id']
        obj = self.get_object(id)
        form = self.get_form()
        payload = {
            'form': form
        }

        if obj is not None:
            form.populate_obj(obj)
        return payload
