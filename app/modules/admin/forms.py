from flask_wtf import Form
from wtforms import StringField


class CategoryForm(Form):
    """
    Form for create/update category
    """
    title = StringField()
