from app import db


class User(db.Document):
    """
    User model
    """
    username = db.StringField(required=True, unique=True, max_length=80)
    email = db.EmailField(required=True, unique=True)
    password = db.StringField()
    first_name = db.StringField(max_length=120)
    last_name = db.StringField(max_length=120)
    created = db.DateTimeField()
    updated = db.DateTimeField()

    meta = {
        'ordering': ['-created'],
        'collection': 'users'
    }
