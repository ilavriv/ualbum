from flask import request
from flask.ext.restful import Resource

from app import api

from . import account
from .models import User


class UsersListResource(Resource):

    def get(self):
        users = User.objects
        return users.to_json()

    def post(self):
        user = User.from_json(request.data)
        user.save()
        return user.to_json(), 204


class UserResource(Resource):

    def get(self, user_id):
        user = User.objects.get(id=user_id)
        return user.to_json()

    def delete(self, user_id):
        return '', 204


api.add_resource(UsersListResource, '/users/')
api.add_resource(UserResource, '/users/<string:user_id>')
