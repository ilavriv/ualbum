var dependencies = [
    'ngRoute', 'ngResource', 
    'Ualbum.utils', 'Ualbum.resources', 'Ualbum.controllers'];

var Ualbum = angular.module('Ualbum', dependencies);

function config ($routeProvider, templateProvider) {
  var templatesUrl = function(t) {
    return '/static/templates/' + t;
  };

  $routeProvider
    .when('/', {
      templateUrl: templatesUrl('core/main.html'),
      controller: 'IndexController'
    })
    .when('/news', {
      templateUrl: templatesUrl('news/index.html'),
      controller: 'NewsController'
    })
    .when('/musicians', {
      templateUrl: templatesUrl('core/faq.html'),
      controller: 'MusiciansController'
    })
    .when('/albums', {
      templateUrl: templatesUrl('albums/index.html'),
      controller: 'AlbumsController'
    })
    .when('/contacts', {
      templateUrl: templatesUrl('core/contacts.html'),
      controller: 'ContactsController'
    })
    .otherwise({
      redirectTo: '/'
    });
};

config.inject = ['$routeProvider', 'templateProvider'];
Ualbum.config(config);
