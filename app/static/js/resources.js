var dependencies = ['ngResource'];
var UalbumResources = angular.module('Ualbum.resources', dependencies);

function User($resource) {
  return $resource('/api/v1/users/', {pk: '@pk', action: '@action'});
}
User.$inject = ['$resource'];

function NewsItem($resource) {
  return $resource('/api/v1/news/', {pk: '@pk', action: '@action'}); 
}
NewsItem.$inject = ['$resource'];

function Category($resource) {
  return $resource('/api/v1/categories/', {pk: '@pk', action: '@action'});
}
Category.$inject = ['$resource'];

UalbumResources
  .factory('User', User)
  .factory('NewsItem', NewsItem)
  .factory('Category', Category);
