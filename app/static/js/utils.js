var dependencies = ['Ualbum.constants'];

var UalbumUtils = angular.module('Ualbum.utils', dependencies);

function template(TEMPLATE_PATH) {
    return function(t) {
        return TEMPLATE_PATH + t;
    };
}

template.$inject = ['TEMPLATE_PATH']

UalbumUtils.factory('template', template);
