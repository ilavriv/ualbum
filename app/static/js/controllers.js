var dependencies = [];
var UalbumControllers = angular.module('Ualbum.controllers', dependencies);


function IndexController($scope, User) {
  User.query(function(data) {
    $scope.data = data;    
  });

  $scope.plans = ['test plan1', 'test plan2', 'test plan3'];
}

IndexController.$inject = ['$scope', 'User'];

function NewsController($scope, Category, NewsItem) {
  Category.query(function(categories) {
    $scope.categories = categories;
  });

  NewsItem.query(function(news) {
    $scope.news = news;
  });
}

NewsController.$inject = ['$scope', 'Category', 'NewsItem'];

function AlbumsController($scope) {

}

AlbumsController.$inject = ['$scope'];

function MusiciansController($scope) {

}

MusiciansController.$inject = ['$scope'];

function ContactsController($scope) {
    
}

ContactsController.$inject = ['$scope'];

UalbumControllers
  .controller('IndexController', IndexController)
  .controller('NewsController', NewsController)
  .controller('MusiciansController', MusiciansController)
  .controller('AlbumsController', AlbumsController)
  .controller('ContactsController', ContactsController);
